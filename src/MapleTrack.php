<?php
use \Sunra\PhpSimple\HtmlDomParser as HtmlDomParser;

class MapleTrack extends Track
{

	private $url = 'http://www.25431010.tw/Search.php';

	public function __construct (Post $post = null)
	{
		parent::__construct($this->url, $post);
	}

	protected function makePostData ($id)
	{
		return [
				"BARCODE1" => $id
		];
	}

	protected function parseResponse ($response)
	{
		$html = HtmlDomParser::str_get_html($response);
		
		$results = [];
		foreach ($html->find('td[bgcolor=#e6eff7]') as $row)
		{
			if (! $row->has_child())
			{
				array_push($results, $row->innertext);
			}
		}
		
		$status = new Status();
		if (! empty($results))
		{
			$status->id = $results[0];
			$status->sentDate = $results[1];
			$status->distributionStatus = $results[2];
		}
		
		$html->clear();
		unset($html);
		
		return $status;
	}
}

?>