<?php
use \Sunra\PhpSimple\HtmlDomParser as HtmlDomParser;

class KerrytjTrack extends Track
{

	private $url = 'http://www.kerrytj.com/zh/search/search_track_list.aspx';

	public function __construct (Post $post = null)
	{
		parent::__construct($this->url, $post);
	}

	protected function makePostData ($id)
	{
		return [
				"rdType" => "0",
				"trackNo1" => $id
		];
	}

	protected function parseResponse ($response)
	{
		$html = HtmlDomParser::str_get_html($response);
		
		$results = [];
		foreach ($html->find('td[bgcolor=#FFFFFF]') as $row)
		{
			if (! $row->has_child())
			{
				array_push($results, $row->innertext);
			}
			else
			{
				array_push($results, $row->children[1]->plaintext);
			}
		}
		
		$status = new Status();
		if (! empty($results))
		{
			$results[3] = $this->convertRepublicEraToAd($results[3]);
			
			$status->id = $results[0];
			$status->startStation = $results[1];
			$status->endStation = $results[2];
			$status->sentDate = $results[3];
			$status->packageNumber = $results[4];
			$status->distributionStatus = $results[5];
		}
		
		$html->clear();
		unset($html);
		
		return $status;
	}

	private function convertRepublicEraToAd ($republicEra)
	{
		$date = new DateTime(str_replace('/', '-', $republicEra));
		$date->modify('+1911 year');
		return $date->format('Y/m/d');
	}
}

?>