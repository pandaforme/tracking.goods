<?php

class CurlPost implements Post
{

	public function post ($url, $data)
	{
		$curl = new Curl();		
		$curl->post($url, $data);
		$response = $curl->response;
		$curl->close();
		
		return $response;
	}
}

?>