<?php

abstract class Track
{

	private $url;

	private $post;

	public function __construct ($url, Post $post = null)
	{
		$this->post = is_null($post) ? new CurlPost() : $post;
		$this->url = $url;
	}

	public function getStatus ($id)
	{
		$postData = $this->makePostData($id);
		$response = $this->post->post($this->url, $postData);
		$results = $this->parseResponse($response);
		
		return json_encode($results);
	}

	protected abstract function makePostData ($id);

	protected abstract function parseResponse ($response);
}

?>