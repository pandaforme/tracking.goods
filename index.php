<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/autoload.php';

use \Slim\Slim as Slim;
use \Flynsarmy\SlimMonolog\Log\MonologWriter as MonologWriter;
use \Monolog\Handler\StreamHandler as StreamHandler;

$app = new Slim(array(
		'log.writer' => getLogWriter()
));

$app->config(getConfig());

$app->get('/:name/:id', 
		function  ($name, $id) use( $app)
		{
			$class = new ReflectionClass($name);
			$track = $class->newInstance(null);
			
			if (is_null($track))
			{
				$app->notFound();
			}
			else
			{
				echo $track->getStatus($id);
			}
		});

$app->run();

function getLogWriter ()
{
	$logPath = __DIR__ . '/logs/';
	if (! file_exists($logPath))
	{
		mkdir($logPath, 755, true);
	}
	
	$logWriter = new MonologWriter([
			'handlers' => [
					new StreamHandler($logPath . date('Y-m-d') . '.log')
			]
	]);
	
	return $logWriter;
}

function getConfig ()
{
	$config = parse_ini_file('config.ini');
	
	return $config;
}

